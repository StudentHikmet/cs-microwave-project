﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TheLegendaryMicrowave
{
    using System.Timers;
    using System.Media;

    public partial class MainWindow : Window
    {
        private ImageSource MicrowaveImgSource = GetItemImage("microwave");
        private ImageSource DoorOpenImgSource = GetItemImage("door_open");
        private ImageSource DoorClosedImgSource = GetItemImage("door_closed");

        private ImageSource OnImageSource = GetItemImage("button-on");
        private ImageSource OffImageSource = GetItemImage("button-off");

        private ImageSource LightOnImageSource = GetItemImage("lightbulb-on");
        private ImageSource LightOffImageSource = GetItemImage("lightbulb-off");

        private List<FoodItem> CurrentFood = new List<FoodItem>();

        private Timer _Timer = new Timer() { AutoReset = true, Interval = 1000 };

        private int _TimeLeft = 15;

        public int TimeLeft
        {
            get => _TimeLeft;
            set
            {
                if (value <= 0)
                {
                    HeatAll();
                    _TimeLeft = 15;
                    TurnedOn = false;
                }
                else _TimeLeft = value;

                Dispatcher.Invoke(() => Counter.Text = TimeLeft.ToString());
            }
        }

        private void OnTimer(Object source, ElapsedEventArgs e) => TimeLeft--;

        private string SelectedFood => ((ComboBoxItem)FoodSelector.SelectedItem).Content.ToString();

        private bool _DoorOpen;

        public bool DoorOpen
        {
            get => _DoorOpen;
            set
            {
                if (value != _DoorOpen && !_Timer.Enabled)
                {
                    if (value)
                        OpenDoor();
                    else CloseDoor();

                    _DoorOpen = value;
                }
            }
        }



        private bool _TurnedOn;

        public bool TurnedOn
        {
            get => _TurnedOn;
            set
            {
                if (value != _TurnedOn && !_DoorOpen)
                {
                    switch (value)
                    {
                        case true:
                            _Timer.Start();
                            break;
                        case false:
                            _TimeLeft = 15;
                            Dispatcher.Invoke(() => Counter.Text = TimeLeft.ToString());
                            _Timer.Stop();
                            break;
                    }

                    Dispatcher.Invoke(() => OnOffButton.Source = value ? OnImageSource : OffImageSource);
                    Dispatcher.Invoke(() => LightBulb.Source =  value ? LightOnImageSource: LightOffImageSource);
                    _TurnedOn = value;
                }
            }
        }

        private void OpenDoor()
        {
            Door.Margin = new Thickness(-550, 0, 0, 0);
            (Door.Width, Door.Height) = (600, 400);

            Door.Source = DoorOpenImgSource;
        }

        private void CloseDoor()
        {
            Door.Margin = new Thickness(-110, -24, 0, 0);
            (Door.Width, Door.Height) = (354, 400);
            Door.Source = DoorClosedImgSource;
        }

        private void DoorSwitch(object sender, MouseEventArgs e) => DoorOpen = !_DoorOpen;

        private void OnOffSwitch(object sender, MouseEventArgs e) => TurnedOn = !_TurnedOn;

        private void AddItem(object sender, RoutedEventArgs e) => CurrentFood.Add(new FoodItem(Microwave, SelectedFood));

        private void RemoveItem(object sender, RoutedEventArgs e)
        {
            FoodItem item = CurrentFood.Find(i => i.FoodName == SelectedFood);
            if (item != null)
            {
                Microwave.Children.Remove(item.FoodImage);
                CurrentFood.Remove(item);
                item = null;
            }
        }

        private void RemoveAll(object sender, RoutedEventArgs e)
        {
            CurrentFood.ForEach(delegate (FoodItem i)
            {
                Microwave.Children.Remove(i.FoodImage);
                i = null;
            });
            CurrentFood.Clear();
        }

        private static ImageSource GetItemImage(string name) => new ImageSourceConverter().ConvertFromString($@"Resources/{name}.png") as ImageSource;

        private void HeatAll()
        {
            if (CurrentFood.Count != 0)
                Dispatcher.Invoke(() => CurrentFood.ForEach(f => f.Heat()));
        }
        public MainWindow()
        {
            InitializeComponent();

            _Timer.Elapsed += OnTimer;

            MicrowaveImg.Source = MicrowaveImgSource;

            OnOffButton.Source = OffImageSource;

            LightBulb.Source = LightOffImageSource;

            CloseDoor();
        }

        internal class FoodItem
        {
            public string FoodName;
            public Image FoodImage = new Image() { Width = 100, Height = 100, Margin = new Thickness(-100, 25, 0, 0) };
            
            public FoodItem(Grid Parent, string Name)
            {
                FoodName = Name;
                FoodImage.Source = GetItemImage(FoodName);

                Grid.SetZIndex(FoodImage, -1);

                Parent.Children.Add(FoodImage);
            }

            public void Heat()
            {
                FoodName = HeatedName(FoodName);
                FoodImage.Source = GetItemImage(FoodName);
            }

            public static string HeatedName(string ToHeat)
            {
                switch (ToHeat)
                {
                    case "potato":
                        return "potato_cooked";

                    case "tomato":
                        return "tomato_cooked";

                    case "onion":
                        return "onion_cooked";

                    case "chicken":
                        return "chicken_cooked";

                    default:
                        return "ash";
                }
            }
        }
    }
}
